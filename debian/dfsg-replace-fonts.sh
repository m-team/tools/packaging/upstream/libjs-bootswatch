#!/bin/bash
# Script to replace google fonts with local versions

FONT_DIR=debian/fonts/

FILE=dist/yeti/bootstrap.css

determine_required_font() {
    # determine font
    FONT_REQUIRED=`grep "https://fonts.googleapis.com/css2?family=" ${1} \
        | sed s%https://fonts.googleapis.com/css2?family=%% \
        | awk -F\" '{ print $2 }'\
        | awk -F: '{ print $1 }'`
    echo "${FONT_REQUIRED}"
}

unsupported () {
    FILE=${1}
    FONT=${2}
    echo "Font ${FONT} in theme ${FILE} is currently not supported."
    echo ${UNSUPPORTED_THEMES} | grep -q `dirname ${FILE}` || {
        UNSUPPORTED_THEMES="${UNSUPPORTED_THEMES} `dirname ${FILE}`"
    }
}

UNSUPPORTED_THEMES=""

# make sure we're on quilt patch "fix-google-fonts"
quilt init
quilt delete fix-google-fonts || true
quilt new fix-google-fonts

for FILE in dist/*/bootstrap.css dist/*/_bootswatch.scss ; do
    FONT_FILE=""
    # echo FILE: $FILE

    FONT=$(determine_required_font $FILE)

    # echo "FONT: ${FONT}"
    case "${FONT}" in
        Lato)               FONT_FILE="lato-debian.css"                 ;;
        News+Cycle)         FONT_FILE="news-cycle-debian.css"           ;;
        Open+Sans)          FONT_FILE="open-sans-debian.css"            ;;
        Roboto)             FONT_FILE="roboto-debian.css"               ;;
        Source+Sans+Pro)    FONT_FILE="source-sans-pro-debian.css"      ;;
        Ubuntu)             FONT_FILE="ubuntu-debian.css"               ;;
        Inter)              unsupported $FILE $FONT                     ;;
        Montserrat)         unsupported $FILE $FONT                     ;;
        Nunito)             unsupported $FILE $FONT                     ;;
        Nunito+Sans)        unsupported $FILE $FONT                     ;;
        *)                  unsupported $FILE $FONT                     ;;
    esac

    # echo "FONT FILE: $FONT_FILE"

    [ -z ${FONT_FILE} ] || {
        quilt add ${FILE}
        sed s%"https://fonts.googleapis.com/css.*\""%"${FONT_FILE}\""% \
            -i ${FILE}
        quilt add `dirname ${FILE}`/${FONT_FILE}
        cp ${FONT_DIR}/${FONT_FILE} `dirname ${FILE}`
    }
# raleway-debian.css: Was used in removed theme "readable"
done
quilt refresh


echo "These themes contain unsupported font and will be removed:"
echo "${UNSUPPORTED_THEMES}"

for DIR in ${UNSUPPORTED_THEMES}; do 
    quilt add ${DIR}/*
    rm -rf ${DIR}
    quilt refresh
done

