%global pkg_name    bootswatch
%global _jsdir     /usr/share/javascript

Name:           %{pkg_name}
Version:        5.2.3
Release:        1
Summary:        bootswatch (XStatic packaging standard)

License:        MIT
URL:            http://bootswatch.com
Source0:        https://github.com/thomaspark/bootswatch/archive/refs/tags/v%{version}.tar.gz
Patch0:         https://salsa.debian.org/marcvs/libjs-bootswatch/-/raw/debian/dev-5.2.3/debian/patches/fix-google-fonts
BuildArch:      noarch
BuildRequires:  yuicompressor

%description
Bootswatch javascript library packaged
for minimal dependecies. With included fonts

Free themes for Bootstrap

Provides:       %{pkg_name} = %{version}-%{release}


%prep
# %setup -q -n %{pkg_name}-%{version}
# %setup -n bootswatch-%{version}
%autosetup -p1
# %setup

# # patch to use webassets dir
# sed -i "s|^BASE_DIR = .*|BASE_DIR = '%{_jsdir}/bootswatch'|" xstatic/pkg/bootswatch/__init__.py
# # License file is present in GitHub repo, but not in PyPi file
# cp %{SOURCE1} .


# %build
# %if %{with python2}
# %{__python2} setup.py build
# %endif
#
# %if %{with python3}
# %{__python3} setup.py build
# %endif

%install
mkdir -p %{buildroot}/%{_datadir}/javascript/bootswatch
chmod a-x dist/*/*
cp -af dist/* %{buildroot}/%{_datadir}/javascript/bootswatch

# rebuild/overwrite bootstrap.min.css files
DESTDIR=%{buildroot}/%{_datadir}/javascript/bootswatch
for i in `find dist -maxdepth 2 -iname bootstrap.css` ; do
    CSSDIRNAME=`dirname ${i}`
    echo "i: ${i}"
    echo "CSSDIRNAME: ${CSSDIRNAME}"
    THEME_NAME=`echo ${CSSDIRNAME} | sed s/dist\\\///`
    echo "THEME_NAME: ${THEME_NAME}"
    echo "DESTDIR: ${DESTDIR}"
    [ -e ${DESTDIR}/$CSSDIRNAME ] || mkdir -p ${DESTDIR}/${THEME_NAME}
    yuicompressor $CSSDIRNAME/bootstrap.css -o ${DESTDIR}/${THEME_NAME}/bootstrap.min.css
done

# rmdir %{buildroot}/%{python2_sitelib}/xstatic/pkg/bootswatch/data/fonts


%files
%{_jsdir}/bootswatch


%changelog
* Mon Jan 09 2023 12:40:31 +0100 Marcus Hardt <marcus@hardt-it.de> - 5.3.2-1
- Packaging upstream version 5.3.2
- Using github as upstream
- Removes python dependencies

* Thu Oct 03 2019 Miro Hrončok <mhroncok@redhat.com> - 3.3.7.0-11
- Rebuilt for Python 3.8.0rc1 (#1748018)

* Mon Aug 19 2019 Miro Hrončok <mhroncok@redhat.com> - 3.3.7.0-10
- Rebuilt for Python 3.8

* Fri Jul 26 2019 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.7.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.7.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Mon Sep 17 2018 Javier Peña <jpena@redhat.com> - 3.3.7.0-7
- Removed Python 2 package from Fedora 30+ (bz#1629755)

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.7.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 3.3.7.0-5
- Rebuilt for Python 3.7

* Fri Feb 09 2018 Iryna Shcherbina <ishcherb@redhat.com> - 3.3.7.0-4
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.7.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.7.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Feb  6 2017 Haïkel Guémar <hguemar@fedoraproject.org> - 3.3.7.0-1
- Upstream 3.3.7.0

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 3.3.6.0-2
- Rebuild for Python 3.6

* Fri Aug 19 2016 jpena <jpena@redhat.com> - 3.3.6.0-1
- Updated to upstream version 3.3.6.0
- Added external license file, it is no longer bundled in source
- Fixed source URL

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.5.3-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.3.5.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.5.3-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Mon Sep 07 2015 jpena <jpena@redhat.com> - 3.3.5.3-2
- Added bootswatch-fonts subpackage.
- Shortened description.
* Fri Sep 04 2015 jpena <jpena@redhat.com> - 3.3.5.3-1
- Initial package.
